# Study Supabase
This is a study of supabase by deploying an astro application.

## Start with supabase locally
```
# Get the code
git clone --depth 1 https://github.com/supabase/supabase
# Go to the docker folder
cd supabase/docker
# Copy the fake env vars
cp .env.example .env
# Start
docker compose up
```
* now you can reach it at localhost:3000

## Start develop astro
* I use asdf to develop so I set the node version throug it (see info into .tool-versions)
```
npm create astro@latest
cd supastro
npx astro add react tailwind
npm add @everybody-gives/ui @tailwindcss/forms postcss
# write your code and
npm run dev
```

## Use supabase
* setup .env and
```
npx supabase login
....
```

# References
* [self host supabase](https://supabase.com/docs/guides/self-hosting/docker) also [video](https://www.youtube.com/watch?v=0bqxrm4PnMA)
* [asdf](https://medium.com/@z-uo/muliple-version-of-kubectl-easy-with-asdf-aa3ff72d6e9): alternative to nvm but that is also not for nodejs
* [astro, react, tailwind and supabase](https://www.aleksandra.codes/astro-supabase)