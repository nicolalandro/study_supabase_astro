export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[]

export interface Database {
  public: {
    Tables: {
      groups: {
        Row: {
          created_at: string | null
          created_by: string | null
          id: string
          name: string | null
        }
        Insert: {
          created_at?: string | null
          created_by?: string | null
          id?: string
          name?: string | null
        }
        Update: {
          created_at?: string | null
          created_by?: string | null
          id?: string
          name?: string | null
        }
      }
      members: {
        Row: {
          group_id: string | null
          id: string
          name: string
          selected_by: string | null
        }
        Insert: {
          group_id?: string | null
          id?: string
          name: string
          selected_by?: string | null
        }
        Update: {
          group_id?: string | null
          id?: string
          name?: string
          selected_by?: string | null
        }
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      draw_name11: {
        Args: {
          groupid: string
          username: string
        }
        Returns: string
      }
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
